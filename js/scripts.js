var arrowup = $('.arrowup');
var scrollDown = $('.scrollDown');
var heroText = $('.hero__desc');

arrowup.on('click', function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
 $(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
            arrowup.fadeIn();
    } else {
        arrowup.fadeOut();
    }
});
scrollDown.on('click', function() {
       $("html, body").animate({ scrollTop: $(document).height() }, "slow");
  return false;
    });


$(function(){
    
 $.fn.center = function () {
    this.css("position","absolute");
    this.css("top", ( $(window).height() - this.height() ) / 2  + "px");
    this.css("left", ( $(window).width() - this.width() ) / 2 + "px");
    return this;
}

    heroText.center();   // on page load div will be centered                                                
    $(window).resize(function(){ // whatever the screen size this
       heroText.center();       // this will make it center when 
    });                          // window resize happens
    
});
